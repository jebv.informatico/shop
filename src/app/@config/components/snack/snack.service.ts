import { Injectable } from '@angular/core';

// Snack
import { MatSnackBar, MatSnackBarRef } from '@angular/material/snack-bar';

// Component
import {SnackComponent} from './snack.component'

@Injectable()
export class SnackService {


  constructor(
    private notificationSnackBar : MatSnackBar,
  ) { }


  // {data,duration,direction,verticalPosition,horizontalPosition}

  openSimpleNotificationSnackBar(message: string = "Mensaje por defecto", closeText: string ="Cerrar")
  {
    this.notificationSnackBar.open(message, 'cerrar', {
      duration: 1000
    })
  }


}
