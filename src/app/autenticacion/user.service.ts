import { Injectable } from '@angular/core';

// HTTP
import { HttpClient } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';

// Interface
import { IUsuario, IUsuarioLog } from '@config/interface/usuario.interface';

// Env
import { environment } from '../../environments/environment';
import { take } from 'rxjs/operators';

function getLocalStorage(): Storage {
  return localStorage;
}

@Injectable({
  providedIn: 'root',
})
export class UserService {
  isLoggedIn = false;

  constructor(private http: HttpClient) {}

  logIn(): Observable<IUsuario[]> {
    return this.http.get<IUsuario[]>(environment.userUrl).pipe(take(1));
  }

  saveOnStorage(name: string, token: string, rol: string, id: number) {
    getLocalStorage().setItem('userName', name);
    getLocalStorage().setItem('token', token);
    getLocalStorage().setItem('rol', rol);
    getLocalStorage().setItem('id', id.toString());
  }

  logOut() {
    getLocalStorage().clear();
    this.isLoggedIn = false;
  }

  getLoggedUser(): IUsuarioLog {
    const user: IUsuarioLog = {
      id: +getLocalStorage().getItem('id')!,
      name: getLocalStorage().getItem('userName')!,
      token: getLocalStorage().getItem('token')!,
      rol: getLocalStorage().getItem('rol')!,
    };

    return user;
  }

  isAdmin(): boolean {
    return this.getLoggedUser().rol === 'administrador';
  }

  getIsLoggedIn(): boolean {
    if (getLocalStorage().getItem('userName')) {
      return true;
    } else return false;
  }
}
