import { Component, OnDestroy, OnInit } from '@angular/core';

// Interface
import { ITableHeader } from '../../@config/interface/table.interface';
import { IProduct, IProductCreate } from '../../@config/interface/producto.interface';

// Service
import { ListService } from './list.service';
import { TablaService } from '@config/components/tabla/tabla.service';
import {UserService} from '../../autenticacion/user.service'

// RXJS
import { Subject, Observable } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';

// route
import { Router } from '@angular/router';

// dialog
import { DialogComponent } from '@config/components/dialog/dialog.component';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

// Message
import { deleteMessage } from '@config/messages/global.messages';

// snack
import { SnackComponent } from '@config/components/snack/snack.component';
import { MatSnackBar } from '@angular/material/snack-bar';

// Store
import {Store} from '@ngrx/store'
import {addToCart} from '@config/store/cart.actions'
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit, OnDestroy {
  $unSubscribe: Subject<boolean> = new Subject();

  cart$: Observable<IProductCreate[]>;

  dialogRef?: MatDialogRef<DialogComponent>;

  header: ITableHeader[] = [
    {
      field: 'name',
      label: 'nombre',
    },
    {
      field: 'description',
      label: 'descripcion',
      position: 'left',
    },
    {
      field: 'price',
      label: 'precio',
      position: 'center',
    },

  ];

  data: IProduct[] = [];

  loadTable = false;

  isAdmin = true;

  constructor(
    private listService: ListService,
    private router: Router,
    private dialog: MatDialog,
    public tableService: TablaService,
    private snackBar: MatSnackBar,
    public usuarioService: UserService,
    private store: Store<{cart: IProductCreate[]}>,
  ) {
    this.cart$ = store.select('cart');

  }

  ngOnInit(): void {
    this.checkAdmin();

    this.loadData();

  }


  openDialog() {
    this.dialogRef = this.dialog.open(DialogComponent, {
      data: {
        value: {
          title: 'Eliminar',
          icon: 'warning',
          description: deleteMessage(
            this.tableService.getSelection().selected.length
          ),
        },
      },
    });
  }

  openSnack(message: string) {
    this.snackBar.openFromComponent(SnackComponent, {
      data: {
        value: {
          message,
        },
      },
      duration: 1 * 1000,
    });
  }

  checkAdmin() {
    if (this.usuarioService.isAdmin()) {
      this.header.push({
        field: 'quantity',
        label: 'cantidad',
        position: 'center',
      });
    }
  }

  loadData(): void {
    this.listService
      .getProducts()
      .pipe(takeUntil(this.$unSubscribe))
      .subscribe((products) => {
        this.data = products;
        this.loadTable = true;
      });
  }

  navigateTo() {
    this.router.navigate(['productos/adicionar']);
  }

  ngOnDestroy(): void {
    this.$unSubscribe.next(true);
    this.$unSubscribe.complete();
  }

  deleteElements() {
    this.openDialog();

    this.dialogRef
      ?.afterClosed()
      .pipe(take(1))
      .subscribe((result: boolean) => {
        if (result) {
          this.tableService.getSelection().selected.forEach((data) => {
            this.data = this.data.filter(
              (dat) => dat.id !== (data as IProduct).id
            );
            this.listService
              .deleteProduct((data as IProduct).id!)
              .pipe(take(1))
              .subscribe();
          });
          this.tableService.reloadTable();
          this.openSnack('Elementos eliminados');
        }
      });
  }
}
