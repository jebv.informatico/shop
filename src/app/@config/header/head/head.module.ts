import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Component
import { HeadComponent } from './head.component';

// Material
import {MatToolbarModule} from '@angular/material/toolbar'; 
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import {MatBadgeModule} from '@angular/material/badge'; 

// Layout
import { FlexLayoutModule } from '@angular/flex-layout';

// Tabla
import {TablaModule} from '@config/components/tabla/tabla.module'

// Router
import {RouterModule} from '@angular/router';

// Cart
import {CartModule} from '../../../venta/cart/cart.module'

@NgModule({
  declarations: [
    HeadComponent
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    FlexLayoutModule,
    RouterModule,
    CartModule,
    // TablaModule,
    MatBadgeModule,
    MatButtonModule
  ],
  exports: [HeadComponent]
})
export class HeadModule { }
