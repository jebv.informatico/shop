import { Component, OnInit, NgModule, Inject } from '@angular/core';

//** Modules
import { CommonModule } from '@angular/common';

// Material
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog'
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

// Component

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent  {

  constructor(
    private selfReference: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any 
  ) { 
  }

  closeMe() {
    this.selfReference.close(false);
  }

  confirmMe() {
    this.selfReference.close(true);
  }

}

@NgModule({
  declarations: [DialogComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    MatIconModule,
    // MatFormFieldModule,
    MatButtonModule,
  ],
})
export class DialogModule {}