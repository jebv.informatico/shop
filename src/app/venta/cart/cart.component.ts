import { Component, OnDestroy, OnInit } from '@angular/core';

// RXJS
import { Observable, Subject } from 'rxjs';

// Store
import { Store } from '@ngrx/store';
import {
  addQuantity,
  removeQuantity,
  restart,
} from '@config/store/cart.actions';

// RXJS
import { take, takeUntil } from 'rxjs/operators';

// Interface
import { ICartProductsCreate } from '@config/interface/cart.interface';
import { IProduct } from '@config/interface/producto.interface';
import { ITableHeader } from '@config/interface/table.interface';

// Dialog
import { MatDialogRef } from '@angular/material/dialog';

// Service
import { CartService } from './cart.service';
import { SnackService } from '@config/components/snack/snack.service';
import { TablaService } from '@config/components/tabla/tabla.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
export class CartComponent implements OnInit, OnDestroy {
  dataToShow: IProduct[] = [];

  total: number = 0;

  $unSubscribe: Subject<unknown> = new Subject();

  cart$: Observable<ICartProductsCreate[]>;

  header: ITableHeader[] = [
    {
      field: 'name',
      label: 'nombre',
      position: 'left',
    },
    {
      field: 'description',
      label: 'descripcion',
      position: 'center',
    },
    {
      field: 'price',
      label: 'precio',
      position: 'center',
    },
    {
      field: 'quantity',
      label: 'cantidad',
      position: 'center',
    },
  ];

  constructor(
    private store: Store<{ cart: ICartProductsCreate[] }>,
    private selfRef: MatDialogRef<CartComponent>,
    private cartService: CartService,
    private snackBar: SnackService,
    private tableService: TablaService,
    private router: Router
  ) {
    this.cart$ = this.store.select('cart');

    this.cart$.pipe(takeUntil(this.$unSubscribe)).subscribe((products) => {
      this.dataToShow = [];
      products.forEach((product) => {
        this.dataToShow.push({
          id: product.product.id,
          description: product.product.description,
          name: product.product.name,
          price: product.product.price,
          quantity: product.quantity,
        });
      });
      this.getTotal();
      this.tableService.reloadTable();
    });
  }

  ngOnDestroy(): void {
    this.$unSubscribe.next();
    this.$unSubscribe.complete();
  }

  ngOnInit(): void {}

  getTotal(): void {
    this.total = 0;
    this.dataToShow.forEach((element) => {
      console.log(element.quantity);
      this.total += element.price * element.quantity;
    });
  }

  createCart() {
    const cartProduct: ICartProductsCreate[] = [];
    this.dataToShow.forEach((data) => {
      cartProduct.push({
        product: {
          description: data.description,
          name: data.name,
          price: data.price,
          id: data.id,
          quantity: 1,
        },
        quantity: data.quantity,
      });
    });
    this.cartService
      .createShop(cartProduct, this.total)
      .pipe(take(1))
      .subscribe(
        (data) => {
          this.store.dispatch(restart());
          this.snackBar.openSimpleNotificationSnackBar(
            'Compra realizada con exito'
          );
          this.router.navigate(['/venta']);
          this.selfRef.close();
        },
        (error) => {
          this.snackBar.openSimpleNotificationSnackBar(
            'No se pudo realizar la compra'
          );
          this.selfRef.close();
        }
      );
  }

  close() {
    this.selfRef.close();
  }
}
