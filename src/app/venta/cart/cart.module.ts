import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// fxLayout
import { FlexLayoutModule } from '@angular/flex-layout';

// Table
import { TablaModule } from '@config/components/tabla/tabla.module';

// component
import { CartComponent } from './cart.component';

// Material
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { MatDialogModule } from '@angular/material/dialog';

// Service
import { CartService } from './cart.service';
import { SnackService } from '@config/components/snack/snack.service';
import { ListService } from '../../producto/list/list.service';
import { CreateService } from '../../producto/create/create.service';

@NgModule({
  declarations: [CartComponent],
  imports: [
    CommonModule,
    TablaModule,
    FlexLayoutModule,
    MatCardModule,
    RouterModule,
    MatDialogModule,
    MatButtonModule,
  ],
  exports: [CartComponent],
  providers: [CartService, SnackService, ListService, CreateService],
})
export class CartModule {}
