import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// component
import { ListComponent } from './list/list.component';

// guard
import { IsAuthGuard } from '@config/guard/is-auth.guard';
import { IsAdminGuard } from '@config/guard/is-admin.guard';
const routes: Routes = [
  {
    path: '',
    component: ListComponent,
    canActivate: [IsAuthGuard],
  },
  {
    path: 'adicionar',
    loadChildren: () =>
      import('./create/create.module').then((m) => m.CreateModule),
    canActivate: [IsAuthGuard, IsAdminGuard],
  },
  {
    path: 'editar',
    loadChildren: () =>
      import('./create/create.module').then((m) => m.CreateModule),
    canActivate: [IsAuthGuard, IsAdminGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductoRoutingModule {}
