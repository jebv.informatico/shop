import { Component, OnInit, OnDestroy } from '@angular/core';

// Dialog
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

// Aux Components
import { CartComponent } from '../../../venta/cart/cart.component';

// Iterface
import { ICartProductsCreate } from '@config/interface/cart.interface';

// Store
import { Store } from '@ngrx/store';

// RXJS
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

// Service
import { UserService } from 'src/app/autenticacion/user.service';

@Component({
  selector: 'app-head',
  templateUrl: './head.component.html',
  styleUrls: ['./head.component.css'],
})
export class HeadComponent implements OnDestroy {
  dialogRef?: MatDialogRef<CartComponent>;

  cantProducts = 0;
  cart$: Observable<ICartProductsCreate[]>;
  $unSubscribe: Subject<unknown> = new Subject();

  constructor(
    private store: Store<{ cart: ICartProductsCreate[] }>,
    private dialog: MatDialog,
    public userService: UserService,
  ) {
    this.cart$ = this.store.select('cart');

    this.cart$.pipe(takeUntil(this.$unSubscribe)).subscribe((data) => {
      this.cantProducts = data.length;
    });
  }

  openDialog() {
    this.dialogRef = this.dialog.open(CartComponent);
  }

  ngOnDestroy(): void {
    this.$unSubscribe.next();
    this.$unSubscribe.complete();
  }
}
