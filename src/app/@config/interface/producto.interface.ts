export interface IProduct {
  id?: number;
  name: string;
  description: string;
  price: number;
  quantity: number;
  originalQ?: number;
  total?: number;
}

export interface IProductShow {
  id: number;
  name: string;
  description: string;
  price: number;
  quantity: number;
  total: number;
  user: string;
}

export interface IProductCreate {
  name: string;
  description: string;
  price: number;
  quantity: number;
}
