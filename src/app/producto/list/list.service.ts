import { Injectable } from '@angular/core';

// HTTP
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

// RXJS
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

// Enviroument
import { environment } from '../../../environments/environment';

// Interface
import { IProduct } from '@config/interface/producto.interface';

@Injectable()
export class ListService {
  reload: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) {}

  getProducts(): Observable<IProduct[]> {
    return this.http.get<any[]>(environment.productUrl).pipe(retry(2));
  }

  getProduct(id: number): Observable<IProduct> {
    return this.http.get<IProduct>(environment.productUrl + `/${id}`);
  }

  deleteProduct(id: number): Observable<any> {
    return this.http.delete(environment.productUrl + `/${id}`);
  }

  reloadTable(reload: boolean){
    this.reload.next(reload);
  }
}
