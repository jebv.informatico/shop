import {
  Component,
  OnInit,
  Input,
  AfterViewInit,
  ViewChild,
  Output,
  EventEmitter,
  OnDestroy,
  ContentChild,
  TemplateRef,
} from '@angular/core';

// CDK
import { DataSource } from '@angular/cdk/table';
import { SelectionModel } from '@angular/cdk/collections';

// Material
import { MatTableDataSource } from '@angular/material/table';

// Interface
import { ITableHeader } from '../../../@config/interface/table.interface';
import { IProduct, IProductCreate } from '@config/interface/producto.interface';

// Table
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

// Route
import { Router } from '@angular/router';

// RXJS
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

// Service
import { TablaService } from './tabla.service';

// Store
import { Store } from '@ngrx/store';
import {
  addQuantity,
  addToCart,
  removeQuantity,
} from '@config/store/cart.actions';

@Component({
  selector: 'app-tabla',
  templateUrl: './tabla.component.html',
  styleUrls: ['./tabla.component.scss'],
})
export class TablaComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() tableHeader: ITableHeader[] = [];
  @Input() dat!: any;
  @Input() showAdminOption: boolean = false;
  @Input() showUpOrDown: boolean = false;
  @Input() activeSelect: boolean = true;
  @Input() showElevation: boolean = true;
  @Input() showOptions: boolean = true;
  @Input() showFooter: boolean = false;


  @Output() valueToDelete = new EventEmitter<number>();

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;


  $unSubscribe = new Subject();

  showTable = false;

  label: string[] = [];

  dataSource!: MatTableDataSource<any>;
  selection = new SelectionModel<unknown>(true, []);

  constructor(
    private router: Router,
    private tableService: TablaService,
    private store: Store<{ cart: IProductCreate[] }>
  ) {}
  ngOnDestroy(): void {
    this.$unSubscribe.next();
    this.$unSubscribe.complete();
  }

  ngAfterViewInit(): void {

    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;

    this.selection.changed
      .pipe(takeUntil(this.$unSubscribe))
      .subscribe((data) => {
        this.tableService.setSelection(this.selection);
      });

  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.dat);

    this.label = this.tableHeader.map((data) => data.field);

    if(this.showOptions) this.label.push('opciones');
    if (this.activeSelect) this.label.unshift('select');

    this.reloadTable();

  }

  navigateToEdit(id: number) {
    this.router.navigate(['/productos/editar/', id]);
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  toggleAllRows() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.dataSource.data);
  }

  deleteEmitter(id: number) {
    this.valueToDelete.emit(id);
  }

  addToCart(product: IProduct) {
    this.store.dispatch(
      addToCart({
        product: product,
      })
    );
  }

  add(id: number) {
    this.store.dispatch(
      addQuantity({
        id: id,
      })
    );
  }

  remove(id: number) {
    this.store.dispatch(
      removeQuantity({
        id: id,
      })
    );

  }

  executeReload() {
    this.tableService.reload.next(true);
  }

  reloadTable() {
    this.tableService.reload
      .pipe(takeUntil(this.$unSubscribe))
      .subscribe((data) => {
        if (data) {
          if (this.selection.selected.length > 0 && this.activeSelect) {
            const id: string[] = this.selection.selected.map(
              (data) => (data as { id: string }).id
            );
            this.dataSource.data = this.dataSource.data.filter(
              (data) => !id.includes(data.id)
            );
          
            this.selection.clear();
          } else if(this.dat.length > 0){
            setInterval(() => {
              this.dataSource.data = this.dat;

            }, 100)
          }
          this.tableService.reload.next(false);
          

        }
      });
  }
}
