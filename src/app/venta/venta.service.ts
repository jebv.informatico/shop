import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IShopCart } from '@config/interface/cart.interface';
import { Observable } from 'rxjs';
import { UserService } from '../autenticacion/user.service';

import {environment} from '../../environments/environment'

@Injectable()
export class VentaService {
  constructor(private http: HttpClient, private user: UserService) { }

  getVenta(): Observable<IShopCart[]> {
    return this.http.get<IShopCart[]>(environment.cart);
  }
}
