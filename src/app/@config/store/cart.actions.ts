import { IProduct } from '@config/interface/producto.interface';
import { createAction, props } from '@ngrx/store';

export const addToCart = createAction('[Cart] add', props<{product: IProduct}>());
export const addQuantity = createAction('[Cart] addQ', props<{id: number}>());
export const removeQuantity = createAction('[Cart] removeQ', props<{id: number}>());
export const restart = createAction('[Cart] restart');



