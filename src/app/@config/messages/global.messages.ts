// Const
export const valueUnderZero = 'El valor debe ser superior a cero';

export const required = 'Campo requerido';

// Functions
export function deleteMessage(cant: number) {
  return cant > 1
    ? '¿Desea eliminiar los elementos seleccionados?'
    : '¿Desea eliminiar el elemento seleccionado?';
}
