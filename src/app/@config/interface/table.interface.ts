export interface ITableHeader {
  label: string;
  field: string;
  width?: number;
  position?: 'right' | 'left' | 'center';
}
