import { IProduct } from './producto.interface';
import { IUsuario } from './usuario.interface';

export interface IShopCart {
  id: number;
  usuario: number;
  userName: string;
  productOnCart: ICartProducts[];
  total: number;
}

export interface IShopCartCreate {
  id?: number;
  usuario: number;
  userName: string;
  productOnCart?: ICartProductsCreate[];
  total: number;
}

export interface ICartProducts {
  id: number;
  product: IProduct;
  quantity: number;
}

export interface ICartProductsCreate {
  id?: number;
  product: IProduct;
  quantity: number;
  originalQ?: number;

}

