import { ICartProductsCreate } from '@config/interface/cart.interface';
import { createReducer, on } from '@ngrx/store';
import {
  addToCart,
  addQuantity,
  removeQuantity,
  restart,
} from './cart.actions';

export const initialState: ICartProductsCreate[] = [];

export const cartReducer = createReducer(
  initialState,
  on(addToCart, (state, product) => {
    let isInArray: boolean = false;

    console.log(product.product);

    state = state.map((data) => {
      if (data.product.id === product.product.id) {
        isInArray = true;
      }
      return data.product.id === product.product.id
        ? {
            ...data,
            quantity:
              data.originalQ! > data.quantity
                ? data.quantity + 1
                : data.quantity,
          }
        : data;
    });

    if (isInArray) {
      return [...state];
    } else {
      return [
        ...state,
        {
          product: product.product,
          quantity: 1,
          originalQ: product.product.quantity,
        },
      ];
    }
  }),
  on(removeQuantity, (state, props) => {
    state = state.map((data) => {
      return data.product.id === props.id
        ? { ...data, quantity: data.quantity - 1 }
        : data;
    });

    state = state.filter((data) => data.quantity > 0);

    return [...state];
  }),
  on(addQuantity, (state, props) => {
    state = state.map((data) => {
      return data.product.id === props.id
        ? {
            ...data,
            quantity:
              data.originalQ! > data.quantity
                ? data.quantity + 1
                : data.quantity,
          }
        : data;
    });

    return [...state];
  }),

  on(restart, (state) => [])
);
