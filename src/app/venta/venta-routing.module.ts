import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IsAuthGuard } from '@config/guard/is-auth.guard';
import { VentaComponent } from './venta.component';

const routes: Routes = [{
  path: '',
  component: VentaComponent,
  canActivate: [IsAuthGuard]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VentaRoutingModule { }
