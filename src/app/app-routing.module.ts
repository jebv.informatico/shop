import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./autenticacion/autenticacion.module').then( m => m.AutenticacionModule),
  },
  {
    path: 'productos',
    loadChildren: () => import('./producto/producto.module').then( m => m.ProductoModule),
  },
  {
    path: 'usuario',
    loadChildren: () => import('./cliente/cliente.module').then( m => m.ClienteModule),
  },
  {
    path: 'venta',
    loadChildren: () => import('./venta/venta.module').then( m => m.VentaModule),
  },
  {
    path: 'logIn',
    redirectTo: ''
  },
  {
    path: 'logOut',
    redirectTo: ''
  },
    {
    path: '**',
    redirectTo: ''
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
