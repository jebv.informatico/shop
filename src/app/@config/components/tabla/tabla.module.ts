import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Component
import { TablaComponent } from './tabla.component';

// Material
import { MatTableModule } from '@angular/material/table';
import { CdkTableModule } from '@angular/cdk/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';

// Flex
import { FlexModule } from '@angular/flex-layout';

// Service
import { TablaService } from './tabla.service';

// FX-Layout
import {FlexLayoutModule} from '@angular/flex-layout'

@NgModule({
  declarations: [TablaComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    FlexModule,
    MatIconModule,
    MatCheckboxModule,
    MatButtonModule,
    CdkTableModule,
  ],
  exports: [TablaComponent],
  providers: [TablaService],
})
export class TablaModule {}
