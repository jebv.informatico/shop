import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Routing
import { ProductoRoutingModule } from './producto-routing.module';

// Component
import { ListComponent } from './list/list.component';

// Tabla
import {TablaModule} from '../@config/components/tabla/tabla.module'

// Service
import {ListService} from './list/list.service'
import {TablaService} from '@config/components/tabla/tabla.service'

// Material
import {MatCardModule} from '@angular/material/card'
import {MatButtonModule} from '@angular/material/button'; 

// HTTP
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { DataService } from '../dataDB/data.service';


@NgModule({
  declarations: [
    ListComponent
  ],
  imports: [
    CommonModule,
    ProductoRoutingModule,
    TablaModule,
    MatCardModule,
    MatButtonModule,
  ],
  providers: [ListService, TablaService]
})
export class ProductoModule { }
