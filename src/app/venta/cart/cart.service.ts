import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserService } from '../../autenticacion/user.service';
import { environment } from '../../../environments/environment';

// Interface
import {
  ICartProductsCreate,
  IShopCart,
  IShopCartCreate,
} from '@config/interface/cart.interface';
import { IProduct } from '@config/interface/producto.interface';
import { take } from 'rxjs/operators';
import { ListService } from 'src/app/producto/list/list.service';
import { CreateService } from 'src/app/producto/create/create.service';
import { TablaService } from '@config/components/tabla/tabla.service';

@Injectable()
export class CartService {
  constructor(
    private http: HttpClient,
    private user: UserService,
    private productList: ListService,
    private productCreate: CreateService,
    private tableService: TablaService
  ) {}

  createShop(shop: ICartProductsCreate[], total: number) {
    const cart: IShopCartCreate = {
      total: total,
      usuario: this.user.getLoggedUser().id,
      userName: this.user.getLoggedUser().name,
    };
    cart.productOnCart = [];
    shop.forEach((element) => {
      cart.productOnCart!.push({
        product: element.product,
        quantity: element.quantity,
      });
      this.productList
        .getProduct(element.product.id!)
        .pipe(take(1))
        .subscribe((prod) => {
          prod.quantity -= element.quantity;
          if (prod.quantity > 0)
            this.productCreate
              .save(prod)
              .pipe(take(1))
              .subscribe((data) => {
                this.tableService.reloadTable();
              });
          else
            this.productList
              .deleteProduct(prod.id!)
              .pipe(take(1))
              .subscribe((data) => {
                this.tableService.reloadTable();

              });
        });
    });
    return this.http.post<IShopCart>(environment.cart, cart);
  }
}
