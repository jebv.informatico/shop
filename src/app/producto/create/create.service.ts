import { Injectable } from '@angular/core';

// HTTP
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

// Interface
import {IProduct, IProductCreate} from '@config/interface/producto.interface'
import { Observable } from 'rxjs';

// Enviroument
import {environment} from '../../../environments/environment'
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CreateService {

  constructor(private http: HttpClient) { }

  save(producto: IProduct): Observable<IProduct>{
    return this.http.post<IProduct>(environment.productUrl, producto);
  }

  getProduct(id: number): Observable<IProduct>{
    return this.http.get<IProduct>(environment.productUrl + `/${id}`);
  }
}
