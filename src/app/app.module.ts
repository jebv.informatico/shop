import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// HTTP
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { DataService } from './dataDB/data.service';

// Animation
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Dialog
import { DialogModule } from '@config/components/dialog/dialog.component';

// Snack
import { SnackModule } from '@config/components/snack/snack.component';

// Interceptor
import { TokenInterceptor } from '@config/interceptor/token.interceptor';

// Header
import { HeadModule } from '@config/header/head/head.module';

// Layout
import { FlexLayoutModule } from '@angular/flex-layout';

// Store
import { StoreModule } from '@ngrx/store';
import { cartReducer } from '@config/store/cart.reducer';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forFeature(DataService, {
      dataEncapsulation: false,
    }),
    StoreModule.forRoot({
      cart: cartReducer
    }),
    BrowserAnimationsModule,
    DialogModule,
    SnackModule,
    FlexLayoutModule,
    HeadModule,
    AppRoutingModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
