import { SelectionModel } from '@angular/cdk/collections';
import { Injectable } from '@angular/core';
import { IProduct } from '@config/interface/producto.interface';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class TablaService {

  reload:BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  selection = new SelectionModel<unknown>(true, []);

  constructor() { }

  getSelection(): SelectionModel<unknown> {
    return this.selection;
  }

  setSelection(selected: SelectionModel<unknown>): void {
    this.selection = selected;
  }

  reloadTable(){
    this.reload.next(true);
  }
}
