import { Component, OnDestroy, OnInit } from '@angular/core';
import { IShopCart } from '@config/interface/cart.interface';
import { IProduct, IProductShow } from '@config/interface/producto.interface';
import { ITableHeader } from '@config/interface/table.interface';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { UserService } from '../autenticacion/user.service';

// Service
import {VentaService} from './venta.service'

@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styleUrls: ['./venta.component.css']
})
export class VentaComponent implements OnInit, OnDestroy {

  $unSubscribe: Subject<unknown> = new Subject();

  dataToShow: IProductShow[] = [];       
  
  total: number = 0;

  header: ITableHeader[] = [
    {
      field: 'name',
      label: 'nombre',
      position: 'left',

    },
    {
      field: 'description',
      label: 'descripcion',
      position: 'left',
    },
    {
      field: 'price',
      label: 'precio',
      position: 'center',
    },
    {
      field: 'quantity',
      label: 'cantidad',
      position: 'center',
    },
    {
      field: 'total',
      label: 'total',
      position: 'center',
    },
  ];

  constructor(private ventaService: VentaService, private usuarioService: UserService) { }
  
  ngOnDestroy(): void {
    this.$unSubscribe.next();
    this.$unSubscribe.complete();
  }

  ngOnInit(): void {
    this.ventaService.getVenta().pipe(takeUntil(this.$unSubscribe)).subscribe(data => {
      data.forEach((element: IShopCart) => {
        if(this.usuarioService.getIsLoggedIn() && !this.usuarioService.isAdmin())
        {
          if(element.usuario === this.usuarioService.getLoggedUser().id){
            this.addToShow(element);
          }
        } else {
          this.addToShow(element);
          this.header.push(
            {
              field: 'user',
              label: 'usuario',
              position: 'left',
            },
          )
        }
      });
    })
  }

  addToShow(element: IShopCart){
    this.total += element.total; 

    console.log(element)

    element.productOnCart.forEach(product => {
      this.dataToShow.push(
        {
          id: product.id,
          description: product.product.description,
          name: product.product.name,
          price: product.product.price,
          quantity: product.quantity,
          total: product.quantity * product.product.price,
          user: element.userName,
        }
      )
    })
  }

}
