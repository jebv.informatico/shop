import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateRoutingModule } from './create-routing.module';
import { CreateComponent } from './create.component';

// Form
import { ReactiveFormsModule } from '@angular/forms';

// Material
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field'; 
import {MatIconModule} from '@angular/material/icon'

// Flex
import {FlexModule} from '@angular/flex-layout'

// Animation
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// HTTP
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

// Data
import { DataService } from 'src/app/dataDB/data.service';

@NgModule({
  declarations: [CreateComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    FlexModule,
    CreateRoutingModule,
  ],
  exports: [CreateComponent],
})
export class CreateModule {}
