import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AutenticacionRoutingModule } from './autenticacion-routing.module';

// Components
import { LoginComponent } from './login/login.component';

// Material
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';

// Layout
import { FlexModule } from '@angular/flex-layout';

// From
import { ReactiveFormsModule } from '@angular/forms';

// Service
import {SnackService} from '@config/components/snack/snack.service'



@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    MatCardModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    FlexModule,
    AutenticacionRoutingModule
  ],
  providers: [
    SnackService
  ]
})
export class AutenticacionModule { }
