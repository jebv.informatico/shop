import { Injectable } from '@angular/core';
import { ICartProducts, IShopCart } from '@config/interface/cart.interface';
import { IProduct } from '@config/interface/producto.interface';
import { IRole } from '@config/interface/rol.interface';
import { IUsuario } from '@config/interface/usuario.interface';

// In Memory DB
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root',
})
export class DataService implements InMemoryDbService {
  constructor() {}

  createDb() {
    const products: IProduct[] = [
      {
        id: 1,
        name: 'Seaman Cap3',
        description:
          'Lorem ipsum . Voluptatem excepturi magnam nostrum dolore recusandae',
        price: 40,
        quantity: 5,
        originalQ: 5
      },
      {
        id: 2,
        name: 'Seaman Cap2',
        description:
          'Lorem ipsum . Voluptatem excepturi magnam nostrum dolore recusandae',
        price: 40,
        quantity: 5,
        originalQ: 5

      },
      {
        id: 3,
        name: 'Seaman Cap1',
        description:
          'Lorem ipsum . Voluptatem excepturi magnam nostrum dolore recusandae',
        price: 40,
        quantity: 5,
        originalQ: 5

      },
    ];

    const rol: IRole[] = [
      {
        id: 1,
        name: 'cliente',
      },
      {
        id: 2,
        name: 'administrador',
      },
    ];

    const user: IUsuario[] = [
      {
        id: 1,
        name: 'usuario1',
        password: '1234',
        rol: rol[0],
        token: "889AZ"
      },
      {
        id: 2,
        name: 'usuario2',
        password: '1234',
        rol: rol[1],
        token: "889AB"

      },
      {
        id: 3,
        name: 'usuario3',
        password: '1234',
        rol: rol[0],
        token: "889AE"
      },
    ];

    const cart: IShopCart[] = [];
    const cartProducts: ICartProducts[] = [];

    return {
      products,
      rol,
      user,
      cart,
      cartProducts,
    };
  }

  genId(data: IProduct[] | IShopCart[] | ICartProducts[]): number {
    return data.length > 0 ? Math.max(...data.map((data: any) => data.id!)) + 1 : 11;
  }
}
