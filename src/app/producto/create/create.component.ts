import { Component, OnInit } from '@angular/core';

// Form
import {
  AbstractControl,
  FormBuilder,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';

// Route
import { ActivatedRoute, Router } from '@angular/router';

// Interface
import { IProduct, IProductCreate } from '@config/interface/producto.interface';

// Messages
import { required, valueUnderZero } from '@config/messages/global.messages';

// RXJS
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

// Service
import { CreateService } from './create.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
})
export class CreateComponent implements OnInit {
  // Messages
  required = required;
  valueUnderZero = valueUnderZero;

  // Unsubscribe
  $unSubscribe: Subject<boolean> = new Subject();

  // Form
  productForm = this.formB.group({
    name: [null, Validators.required],
    description: [null, Validators.required],
    quantity: [
      0,
      {
        validators: [Validators.required, this.numberValidator()],
      },
    ],
    price: [
      0,
      {
        validators: [Validators.required, this.numberValidator()],
      },
    ],
  });

  constructor(
    private formB: FormBuilder,
    private createService: CreateService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.get('id')) {
      this.createService
        .getProduct(+this.route.snapshot.paramMap.get('id')!)
        .pipe(takeUntil(this.$unSubscribe))
        .subscribe((data) => {
          this.name.setValue(data.name);
          this.description.setValue(data.description);
          this.price.setValue(data.price);
          this.quantity.setValue(data.quantity);
        });
    }
  }

  save() {
    const product: IProduct = {
      name: this.name.value,
      description: this.description.value,
      price: this.price.value,
      originalQ: this.quantity.value,
      quantity: this.quantity.value,
    };
    if (this.route.snapshot.paramMap.get('id')) {
      product.id = +this.route.snapshot.paramMap.get('id')!;
    }
    this.createService
      .save(product)
      .pipe(takeUntil(this.$unSubscribe))
      .subscribe((data) => {
        console.log(data);
        this.router.navigate(['/productos']);
      });
  }

  close() {
    this.router.navigate(['/productos']);
  }

  get name() {
    return this.productForm.controls['name'];
  }

  get description() {
    return this.productForm.controls['description'];
  }

  get quantity() {
    return this.productForm.controls['quantity'];
  }

  get price() {
    return this.productForm.controls['price'];
  }

  private numberValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (!control.value) {
        return null;
      }

      console.log(control.value);

      return control.value <= 0 ? { underZero: true } : null;
    };
  }
}
