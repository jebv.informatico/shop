import { IRole } from './rol.interface';

export interface IUsuario {
  id: number;
  name: string;
  password: string;
  rol: IRole;
  token: string;
}

export interface IUsuarioLog {
  id: number;
  name: string;
  rol: string;
  token: string;
}
