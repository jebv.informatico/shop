import { Component, OnInit } from '@angular/core';

// Form
import { FormBuilder, Validators } from '@angular/forms';

// Service
import { UserService } from '../user.service';
import { SnackService } from '@config/components/snack/snack.service';

// Message
import { required } from '@config/messages/global.messages';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  required = required;

  logInForm = this.formB.group({
    userName: ['', Validators.required, ,],

    password: ['', Validators.required],
  });

  constructor(
    private formB: FormBuilder,
    public loginService: UserService,
    private router: Router,
    private snackBar: SnackService
  ) {}

  ngOnInit(): void {}

  get userName() {
    return this.logInForm.controls['userName'];
  }

  get password() {
    return this.logInForm.controls['password'];
  }

  logIn() {
    this.loginService.logIn().subscribe(
      (data) => {
        let find: boolean = false;
        for (const iterator of data) {
          if (
            iterator.name === this.userName.value &&
            iterator.password === this.password.value
          ) {
            find = true;
            this.loginService.saveOnStorage(
              iterator.name,
              iterator.token,
              iterator.rol.name,
              iterator.id
            );
            this.router.navigate(['/productos/']);
            break;
          }
        }
        !find &&
          this.snackBar.openSimpleNotificationSnackBar(
            'Usuario o constraseña incorrecto',
            'cerrar'
          );
      },
      (error) => {}
    );
  }

  logOut() {
    this.loginService.logOut();
  }
}
