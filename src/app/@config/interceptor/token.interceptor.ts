import { Injectable } from '@angular/core';

// HTTP
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';

// RXJS
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

// Router
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';

// Service
import { UserService } from '../../autenticacion/user.service';

// SnackBar
import { SnackComponent } from '@config/components/snack/snack.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(
    private router: Router,
    private userService: UserService,
    private snackBar: MatSnackBar
  ) {}

  // Intercepts all the HTTP request
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // Set the auth token
    if (this.userService.getIsLoggedIn()) {
      request = request.clone({
        setHeaders: {
          Authorization: this.userService.getLoggedUser().token,
        },
      });
    }

    // What do after request
    return next.handle(request).pipe(
      // In case of error
      catchError((error) => {
        let handled = false;
        console.log(error);

        console.log(`Status code error: ${error.status}`);
        switch (error.status) {
          // Here goes all the posible error code
          case 0:
          case 400:
          case 200:
          case 404:
            this.createSnackMessage(error.status, error.statusText);
            this.router.navigate([''])
            handled = true;
            break;
        }

        if (handled) {
          console.log('return back ');
          return of(error);
        } else {
          console.log('throw error back to to the subscriber');
          return throwError(error);
        }
      })
    );
  }

  createSnackMessage(error: number, statusText: string) {
    this.snackBar.openFromComponent(SnackComponent, {
      data: {
        value: {
          message: `Error ${error} - ${statusText}`,
        },
      },
      duration: 2 * 1000,
    });
  }
}
