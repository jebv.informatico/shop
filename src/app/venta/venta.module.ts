import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VentaRoutingModule } from './venta-routing.module';

// Component
import { VentaComponent } from './venta.component';

// Material
import {MatCardModule} from '@angular/material/card'; 

// Table
import { TablaModule } from '@config/components/tabla/tabla.module';

// Service
import { VentaService } from './venta.service';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [VentaComponent],
  imports: [CommonModule, TablaModule, MatCardModule, FlexLayoutModule, VentaRoutingModule],
  providers: [VentaService],
})
export class VentaModule {}
