import { Component, OnInit, NgModule, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';

// Material
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import {MatSnackBarModule, MatSnackBarRef, MAT_SNACK_BAR_DATA} from '@angular/material/snack-bar'
import { FlexLayoutModule } from '@angular/flex-layout';

@Component({
  selector: 'app-snack',
  templateUrl: './snack.component.html',
  styleUrls: ['./snack.component.css']
})
export class SnackComponent {

  constructor(
    private selfRef : MatSnackBarRef<SnackComponent>,
    @Inject(MAT_SNACK_BAR_DATA) public data : any,
  ) { }

  closeMe() {
    this.selfRef.dismiss()
  }

}

@NgModule({
  declarations: [SnackComponent],
  imports: [
    CommonModule,
    MatSnackBarModule,
    MatIconModule,
    FlexLayoutModule,
    // MatFormFieldModule,
    MatButtonModule,
  ],
})
export class SnackModule {}